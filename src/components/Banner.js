import React, { Component } from 'react'
import Like from '../assets/svg/Like'
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import ComputerIcon from '@material-ui/icons/Computer';
import PhoneAndroidIcon from '@material-ui/icons/PhoneAndroid';

class Banner extends Component {
  render() {
    return (
      <div className="banner">
        <p className="breadcrumb">Home » Goals » Explorer ›</p>
        <div className="title-wrap">
          <div className="title-recommended">
            <div className="title">Trip to the Palace of Versailles</div>
            <div className="recommended-box">
              <Like />
              <div>
                <strong>125</strong>
                <p>Recommended</p>
              </div>
            </div>
          </div>
          <div className="year-text">
            <span>ONLINE MUSEUM</span>
            <span className="year">8+ Years</span>
          </div>
        </div>
        <div className="main-banner">
          <div className="banner-img">
            <div className="watch-trailor-btn">
              <PlayArrowIcon />
              Watch Trailor
              </div>
          </div>
          <div className="banner-details">
            <div className="live-on-device">
              <div className="live-on">
                <p>Live on</p>
                <strong>29 June 2020 for 60 min</strong>
              </div>
              <div className="device">
                <p>Devices</p>
                <div className="device-icon">
                  <ComputerIcon />{'\n'}<span>or</span>{'\n'}<PhoneAndroidIcon />
                </div>
              </div>
            </div>
            <br />
            <div className="time-slot">
              <p>Choose Slot</p>
              <div className="slots">
                <div className="slot-avail">
                  <div className="slot-timming">
                    10:30 AM
                  </div>
                  2/5 Seat Left
                </div>
                <div className="slot-avail">
                  <div className="slot-timming">
                    5:00 AM
                  </div>

                </div>
                <div className="slot-avail">
                  <div className="slot-timming">
                    06:30 AM
                  </div>
                  1/5 Seat Left
                </div>
              </div>
            </div>
            <div className="fee-section">
              <p>Class Fee</p>
              <div className="price-earn">
                <div className="price-box">
                  <span>INR 120</span>
                  <strong>INR 60</strong>
                </div>
                <p className="earn-points">Earn 100+ Points</p>
              </div>
            </div>
            <div className="cta-btn">
              <div className="book-cta">Book This class for free</div>
              <div className="share-cta">Share</div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Banner
