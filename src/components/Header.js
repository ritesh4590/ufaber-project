import React, { Component } from 'react'
import '../App.css'
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

class Header extends Component {
  render() {
    return (
      <div className="header">
        <div className="header-two">
          <div className="menuIcon">
            <MenuIcon />
          </div>
          <div className="header-inner">
            <div className="icon-logo">
              <h1>REAL SCHOOL</h1>
            </div>
            <div className="nav-profile">
              <nav className="navbar">
                <a>MasterClasses</a>
                <a>Programs</a>
                <a>Challenges</a>
                <a>Library</a>
              </nav>
              <div className="profile-avatar">
                <div className="avatar">
                  <AccountCircleIcon fontSize={'large'} />
                </div>
                <div className="profile">
                  INR 200
              </div>
              </div>
              <div className="right-menu">
                <MenuIcon />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Header
