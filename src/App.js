import React from 'react';
import './App.css';
import Header from './components/Header'
import Banner from './components/Banner'

function App() {
  return (
    <div >
      <Header />
      <div className="App">
        <Banner />
      </div>
    </div>
  );
}

export default App;
